﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace remove
{
    public partial class Form1 : Form
    {
        SqlConnection con1, con2;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            con2 = new SqlConnection("Data Source=DESKTOP-7MRDTE0\\SQLEXPRESS2014;Initial Catalog=DB2;User ID=sa;Password=Vikrant@SQL");
            con2.Open();
            try
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter(" WITH TempEmp (Name,duplicateRecCount) AS (SELECT Id, ROW_NUMBER() OVER(PARTITION by Id, Name ORDER BY Name)AS duplicateRecCount FROM dbo.TBL2) DELETE FROM TempEmp WHERE duplicateRecCount > 1 ", con2 );
                SqlCommandBuilder cb = new SqlCommandBuilder(da);
                DataTable dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                Console.WriteLine("{0}: exception caught", ex.Message);
            }
            MessageBox.Show("Duplicate data has been removed successfully");
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            con1 = new SqlConnection("Data Source=DESKTOP-7MRDTE0\\SQLEXPRESS2014;Initial Catalog=DB1;User ID=sa;Password=Vikrant@SQL");
            con1.Open();
            con2 = new SqlConnection("Data Source=DESKTOP-7MRDTE0\\SQLEXPRESS2014;Initial Catalog=DB2;User ID=sa;Password=Vikrant@SQL");
            con2.Open();
            try
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter("select * from TBL1", con1);
                SqlCommandBuilder cb = new SqlCommandBuilder(da);
                DataTable dt = new DataTable();
                da.Fill(dt);
                int i;

                foreach (DataRow dr in dt.Rows)
                {
                    cmd = new SqlCommand("insert into TBL2 values('" + dr[0].ToString() + "','" + dr[1].ToString() + "', '" + dr[2].ToString() + "')", con2);
                    i = cmd.ExecuteNonQuery();
                    dr[3] = true;

                }
                da.Update(dt);
                cmd.Dispose();
                da.Dispose();
                dt.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0}: exception caught.", ex.Message);
            }
            MessageBox.Show("Data Updated Successfully");
            this.Close();
        }

    }

}


